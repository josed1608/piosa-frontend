import { Component, OnInit } from '@angular/core';
import {BaseService} from '../../base.service';

@Component({
  selector: 'app-recent-articles',
  templateUrl: './recent-articles.component.html',
  styleUrls: ['./recent-articles.component.css']
})
export class RecentArticlesComponent implements OnInit {

  content = [];

  constructor(private baseService: BaseService) {
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.baseService.getArticles(2)
      .subscribe(
        (results: any[]) => {
          this.content = results;
        });
  }

}
