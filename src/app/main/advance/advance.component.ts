import {Component, OnInit} from '@angular/core';
import {BaseService} from '../../base.service';

@Component({
  selector: 'app-advance',
  templateUrl: './advance.component.html',
  styleUrls: ['./advance.component.css']
})
export class AdvanceComponent implements OnInit {

  currentObj: any;
  content = [];

  constructor(private baseService: BaseService) {
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.baseService.getAdvances()
      .subscribe(
        (results: any[]) => {
          this.content = results;
          if (this.content != null && this.content.length > 0) {
            this.currentObj = this.content[0];
          }
        });
  }

}
