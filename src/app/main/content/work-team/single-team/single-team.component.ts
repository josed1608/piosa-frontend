import {Component, OnInit} from '@angular/core';
import {BaseService} from '../../../../base.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-single-team',
  templateUrl: './single-team.component.html',
  styleUrls: ['./single-team.component.css']
})
export class SingleTeamComponent implements OnInit {

  object: any;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private baseService: BaseService) {
  }


  ngOnInit() {
    this.object = this.baseService.selectedObject;
    console.log(this.object);
    if (this.object == null) {
      this.router.navigate(['/team'], {
        relativeTo: this.route,
      });
    }
  }

  getFullName(object: any) {
    if (object != null) {
      let str = '';

      if (object.name != null) {
        str += object.name + ' ';
      }

      if (object.last_name != null) {
        str += object.last_name + ' ';
      }

      if (object.sur_name != null) {
        str += object.sur_name;

      }
      return str;
    }
  }


}
