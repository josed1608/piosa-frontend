import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialActorsComponent } from './social-actors.component';

describe('SocialActorsComponent', () => {
  let component: SocialActorsComponent;
  let fixture: ComponentFixture<SocialActorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialActorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialActorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
