import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSocialActorComponent } from './single-social-actor.component';

describe('SingleSocialActorComponent', () => {
  let component: SingleSocialActorComponent;
  let fixture: ComponentFixture<SingleSocialActorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSocialActorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSocialActorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
