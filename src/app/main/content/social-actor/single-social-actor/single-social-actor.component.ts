import {Component, OnInit} from '@angular/core';
import {BaseService} from '../../../../base.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-single-social-actor',
  templateUrl: './single-social-actor.component.html',
  styleUrls: ['./single-social-actor.component.css']
})
export class SingleSocialActorComponent implements OnInit {

  projects: any[];
  object: any;


  constructor(private router: Router,
              private route: ActivatedRoute,
              private baseService: BaseService) {
  }


  ngOnInit() {
    this.object = this.baseService.selectedObject;
    if (this.object != null) {
      this.loadProjects();
    }
    this.route.queryParams
      .subscribe(
        (queryParams: Params) => {
          const id = queryParams['id'];
          console.log(id);
          if (id != null) {
            this.baseService.getEntityById('socialActor', id).subscribe(
              (data: any) => {
                if (data != null && data.length > 0) {
                  this.object = data[0];
                  console.log(this.object);
                  this.loadProjects();

                }
              });
          }
        });
  }

  getFullName() {
    if (this.object != null) {
      let str = '';

      if (this.object.name != null) {
        str += this.object.name + ' ';
      }

      if (this.object.last_name != null) {
        str += this.object.last_name + ' ';
      }

      if (this.object.sur_name != null) {
        str += this.object.sur_name;

      }
      return str;
    }
  }


  loadProjects() {
    this.baseService.getProjectsByCollaborator(this.object.id)
      .subscribe(
        (results: any[]) => {
          this.projects = results;
        });
  }

}
