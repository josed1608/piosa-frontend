import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BaseService} from '../../../base.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  formSent = false;
  contactForm: FormGroup;

  constructor(private baseService: BaseService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {

    this.contactForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      subject: ['', [Validators.required]],
      message: ['', Validators.required],
    });
  }

  onSendContactForm() {
    this.baseService.sendMailContactForm(this.contactForm.value)
      .subscribe(
        (response) => {
          console.log(response);
          this.formSent = true;
        },
        (error) => {
          console.log(error);
        }
      );
  }

}
