import {Component, OnInit} from '@angular/core';
import {BaseService} from '../../../../base.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-single-investigation',
  templateUrl: './single-investigation.component.html',
  styleUrls: ['./single-investigation.component.css']
})
export class SingleInvestigationComponent implements OnInit {

  object: any;
  coordinators: any[];
  collaborators: any[];

  zoom = 10.45;
  mapLat = 8.55;
  mapLng = -83.5;

  markerTitle = 'PiOSA';
  markerLat = 8.574693;
  markerLng = -83.458139;


  constructor(private router: Router,
              private route: ActivatedRoute,
              private baseService: BaseService) {
  }


  ngOnInit() {
    this.object = this.baseService.selectedObject;
    if (this.object != null) {
      this.loadCoordinators();
      this.loadCollaborators();
    }

    this.route.queryParams
      .subscribe(
        (queryParams: Params) => {
          const id = queryParams['id'];
          console.log(id);
          if (id != null) {
            this.baseService.getEntityById('project', id).subscribe(
              (data: any) => {
                if (data != null && data.length > 0) {
                  this.object = data[0];
                  console.log(this.object);
                  if (this.object != null) {
                    this.loadCoordinators();
                    this.loadCollaborators();
                  }
                }
              });
          }
        });
  }

  loadCollaborators() {
    this.baseService.getCollaboratorsProject(this.object.id)
      .subscribe(
        (results: any[]) => {
          this.collaborators = results;
        });
  }

  loadCoordinators() {
    this.baseService.getCoordinatorsByProject(this.object.id)
      .subscribe(
        (results: any[]) => {
          this.coordinators = results;
        });
  }

  getFullName(object: any) {
    if (object != null) {
      let str = '';

      if (object.name != null) {
        str += object.name + ' ';
      }

      if (object.last_name != null) {
        str += object.last_name + ' ';
      }

      if (object.sur_name != null) {
        str += object.sur_name;

      }
      return str;
    }
  }

}
