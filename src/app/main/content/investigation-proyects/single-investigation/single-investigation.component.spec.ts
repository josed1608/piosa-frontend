import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleInvestigationComponent } from './single-investigation.component';

describe('SingleInvestigationComponent', () => {
  let component: SingleInvestigationComponent;
  let fixture: ComponentFixture<SingleInvestigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleInvestigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleInvestigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
