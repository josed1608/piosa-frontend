import {Component, OnInit} from '@angular/core';
import {BaseService} from '../../../../base.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-investigation',
  templateUrl: './investigation.component.html',
  styleUrls: ['./investigation.component.css']
})
export class InvestigationComponent implements OnInit {

  content = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private baseService: BaseService) {
  }

  ngOnInit() {
    this.loadProyects();
  }

  loadProyects() {
    this.baseService.getProyectsByType(2)
      .subscribe(
        (results: any[]) => {
          this.content = results;
        });
  }

  onClickDetails(obj: any) {
    this.baseService.selectedObject = obj;
    this.router.navigate(['single'], {
      relativeTo: this.route,
      queryParams: {id: obj.id},
    });
  }

}
