import {Component, OnInit} from '@angular/core';
import {BaseService} from '../../../../base.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-single-professor',
  templateUrl: './single-professor.component.html',
  styleUrls: ['./single-professor.component.css']
})
export class SingleProfessorComponent implements OnInit {

  object: any;

  projects: any[];
  academicUnits: any[];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private baseService: BaseService) {
  }


  ngOnInit() {
    this.object = this.baseService.selectedObject;
    if (this.object != null) {
      this.loadProjects();
      this.loadUnits();
    }


    this.route.queryParams
      .subscribe(
        (queryParams: Params) => {
          const id = queryParams['id'];
          console.log(id);
          if (id != null) {
            this.baseService.getEntityById('professor', id).subscribe(
              (data: any) => {
                if (data != null && data.length > 0) {
                  this.object = data[0];
                  console.log(this.object);
                  if (this.object != null) {
                    this.loadProjects();
                    this.loadUnits();
                  }

                }
              });
          }
        });
  }


  loadProjects() {
    this.baseService.getProjectsByProfessor(this.object.id)
      .subscribe(
        (results: any[]) => {
          this.projects = results;
        });
  }

  loadUnits() {
    this.baseService.getUnitsByProfessor(this.object.id)
      .subscribe(
        (results: any[]) => {
          this.academicUnits = results;
        });
  }

  getFullName() {
    if (this.object != null) {
      let str = '';

      if (this.object.name != null) {
        str += this.object.name + ' ';
      }

      if (this.object.last_name != null) {
        str += this.object.last_name + ' ';
      }

      if (this.object.sur_name != null) {
        str += this.object.sur_name;

      }
      return str;
    }
  }

}
