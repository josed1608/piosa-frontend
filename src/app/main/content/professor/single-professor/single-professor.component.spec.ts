import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleProfessorComponent } from './single-professor.component';

describe('SingleProfessorComponent', () => {
  let component: SingleProfessorComponent;
  let fixture: ComponentFixture<SingleProfessorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleProfessorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleProfessorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
