import {Component, OnInit} from '@angular/core';
import {BaseService} from '../../../../base.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-professors',
  templateUrl: './professors.component.html',
  styleUrls: ['./professors.component.css']
})
export class ProfessorsComponent implements OnInit {

  content = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private baseService: BaseService) {
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.baseService.getProfessors()
      .subscribe(
        (results: any[]) => {
          this.content = results;
        });
  }


  onClickDetails(obj: any) {
    this.baseService.selectedObject = obj;
    this.router.navigate(['single'], {
      relativeTo: this.route,
      queryParams: {id: obj.id},
    });
  }


  getFullName(object: any) {
    if (object != null) {
      let str = '';

      if (object.name != null) {
        str += object.name + ' ';
      }

      if (object.last_name != null) {
        str += object.last_name + ' ';
      }

      if (object.sur_name != null) {
        str += object.sur_name;

      }
      return str;
    }
  }
}
