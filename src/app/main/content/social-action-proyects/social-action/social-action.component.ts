import {Component, OnInit} from '@angular/core';
import {BaseService} from '../../../../base.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-social-action',
  templateUrl: './social-action.component.html',
  styleUrls: ['./social-action.component.css']
})
export class SocialActionComponent implements OnInit {

  content = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private baseService: BaseService) {
  }

  ngOnInit() {
    this.loadProyects();
  }

  loadProyects() {
    this.baseService.getProyectsByType(1)
      .subscribe(
        (results: any[]) => {
          this.content = results;
        });
  }


  onClickDetails(obj: any) {
    this.baseService.selectedObject = obj;
    this.router.navigate(['single'], {
      relativeTo: this.route,
      queryParams: {id: obj.id},
    });
  }


}
