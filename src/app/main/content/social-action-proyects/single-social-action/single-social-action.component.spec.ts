import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSocialActionComponent } from './single-social-action.component';

describe('SingleSocialActionComponent', () => {
  let component: SingleSocialActionComponent;
  let fixture: ComponentFixture<SingleSocialActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSocialActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSocialActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
