import {Component, OnInit} from '@angular/core';
import {BaseService} from '../../../base.service';
import {CalendarEvent} from 'angular-calendar';
import {isSameDay, isSameMonth} from 'date-fns';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  zoom = 10.45;
  mapLat = 8.55;
  mapLng = -83.5;

  markerTitle = 'PiOSA';
  markerLat = 8.574693;
  markerLng = -83.458139;

  markerLng2 = -83.513191;
  markerLat2 = 8.565376;

  markerLng3 = -83.305550;
  markerLat3 = 8.421022;

  articles = [];
  view = 'month';

  viewDate: Date = new Date();
  localeLang = 'es';

  colors = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3'
    },
    blue: {
      primary: '#41ADE7',
      secondary: '#D1E8FF'
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA'
    }
  };

  events: CalendarEvent[] = [];

  activeDayIsOpen =  false;

  constructor(private baseService: BaseService) {
  }

  ngOnInit() {
    this.loadArticles();

    this.loadEvents();

  }

  loadArticles() {
    this.baseService.getArticles(5)
      .subscribe(
        (results: any[]) => {
          this.articles = results;
          console.log(results);
        });
  }


  loadEvents() {
    this.baseService.getEvents()
      .subscribe(
        (results: any[]) => {

          const tmp = [];
          for (const obj of results) {
            tmp.push({
              start: new Date(obj.start),
              end: new Date(obj.end),
              title: obj.title,
              colors: this.colors.blue
            });
          }
          this.events = tmp;
        });
  }

  dayClicked({
               date,
               events
             }: {
    date: Date;
    events: Array<CalendarEvent>;
  }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  onChangeView(view: string) {
    this.view = view;

  }


}
