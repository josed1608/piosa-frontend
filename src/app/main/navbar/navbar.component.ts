import { Component, OnInit } from '@angular/core';


declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  {path: '/home', title: 'Inicio', icon: 'home', class: ''},
  {path: '/about', title: 'Acerca de PiOSA', icon: 'tag', class: ''},
  {path: '/social', title: 'Acción Social', icon: 'hashtag', class: ''},
  {path: '/investigation', title: 'Investigación', icon: 'futbol-o', class: ''},
  {path: '/professors', title: 'Docentes', icon: 'flag', class: ''},
  {path: '/team', title: 'Equipos de Trabajo', icon: 'flag', class: ''},
  {path: '/social-actors', title: 'Actores Sociales', icon: 'futbol-o', class: ''},
  {path: '/contact', title: 'Contacto', icon: 'futbol-o', class: ''},
];


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }

}
