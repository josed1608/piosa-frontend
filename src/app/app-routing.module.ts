import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './main/content/home/home.component';
import {SocialActionComponent} from './main/content/social-action-proyects/social-action/social-action.component';
import {AboutComponent} from './main/content/about/about.component';
import {TeamComponent} from './main/content/work-team/team/team.component';
import {ContactComponent} from './main/content/contact/contact.component';
import {InvestigationComponent} from './main/content/investigation-proyects/investigation/investigation.component';
import {ProfessorsComponent} from './main/content/professor/professors/professors.component';
import {SocialActorsComponent} from './main/content/social-actor/social-actors/social-actors.component';
import {SingleSocialActionComponent} from './main/content/social-action-proyects/single-social-action/single-social-action.component';
import {SingleInvestigationComponent} from './main/content/investigation-proyects/single-investigation/single-investigation.component';
import {SingleProfessorComponent} from './main/content/professor/single-professor/single-professor.component';
import {SingleTeamComponent} from './main/content/work-team/single-team/single-team.component';
import {SingleSocialActorComponent} from './main/content/social-actor/single-social-actor/single-social-actor.component';

const routes: Routes = [

    // Home
    {path: 'home', component: HomeComponent},

    // About
    {path: 'about', component: AboutComponent},

    // Social Projects
    {path: 'social', component: SocialActionComponent},
    {path: 'social/:id', component: SingleSocialActionComponent},

    // Investigation Projects
    {path: 'investigation', component: InvestigationComponent},
    {path: 'investigation/:id', component: SingleInvestigationComponent},

    // Professors
    {path: 'professors', component: ProfessorsComponent},
    {path: 'professors/:id', component: SingleProfessorComponent},

    // Team
    {path: 'team', component: TeamComponent},
    {path: 'team/:id', component: SingleTeamComponent},

    // Social Actors
    {path: 'social-actors', component: SocialActorsComponent},
    {path: 'social-actors/:id', component: SingleSocialActorComponent},

    // Contact
    {path: 'contact', component: ContactComponent},

    {path: '**', redirectTo: 'home'}
  ]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

