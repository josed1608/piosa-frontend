import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BsDropdownModule, ModalModule, TooltipModule} from 'ngx-bootstrap';
import {NgbAlertModule, NgbCarouselModule, NgbDropdownModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MainComponent} from './main/main.component';
import {HomeComponent} from './main/content/home/home.component';
import {NavbarComponent} from './main/navbar/navbar.component';
import {TopHeaderComponent} from './main/top-header/top-header.component';
import {MainCarouselComponent} from './main/main-carousel/main-carousel.component';
import {AnnouncementsComponent} from './main/announcements/announcements.component';
import {RecentArticlesComponent} from './main/recent-articles/recent-articles.component';
import {FooterComponent} from './main/footer/footer.component';
import {AppRoutingModule} from './app-routing.module';
import {AboutComponent} from './main/content/about/about.component';
import {SocialActionComponent} from './main/content/social-action-proyects/social-action/social-action.component';
import {InvestigationComponent} from './main/content/investigation-proyects/investigation/investigation.component';
import {ProfessorsComponent} from './main/content/professor/professors/professors.component';
import {TeamComponent} from './main/content/work-team/team/team.component';
import {ContactComponent} from './main/content/contact/contact.component';
import {MapComponent} from './main/components/map/map.component';
import {CalendarComponent} from './main/components/calendar/calendar.component';
import {SocialActorsComponent} from './main/content/social-actor/social-actors/social-actors.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AdvanceComponent} from './main/advance/advance.component';
import {BaseService} from './base.service';

import { AgmCoreModule } from '@agm/core';
import {CalendarModule} from 'angular-calendar';
import { SingleSocialActorComponent } from './main/content/social-actor/single-social-actor/single-social-actor.component';
import { SingleProfessorComponent } from './main/content/professor/single-professor/single-professor.component';
import { SingleInvestigationComponent } from './main/content/investigation-proyects/single-investigation/single-investigation.component';
import { SingleSocialActionComponent } from './main/content/social-action-proyects/single-social-action/single-social-action.component';
import { SingleTeamComponent } from './main/content/work-team/single-team/single-team.component';

import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
registerLocaleData(localeFr);
registerLocaleData(localeEs);

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HomeComponent,
    NavbarComponent,
    TopHeaderComponent,
    MainCarouselComponent,
    AnnouncementsComponent,
    RecentArticlesComponent,
    FooterComponent,
    AboutComponent,
    SocialActionComponent,
    InvestigationComponent,
    ProfessorsComponent,
    TeamComponent,
    ContactComponent,
    MapComponent,
    CalendarComponent,
    SocialActorsComponent,
    AdvanceComponent,
    SingleSocialActorComponent,
    SingleProfessorComponent,
    SingleInvestigationComponent,
    SingleSocialActionComponent,
    SingleTeamComponent,
  ],
  imports: [
    BrowserModule,
    BsDropdownModule.forRoot(),
    AppRoutingModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    NgbModule.forRoot(),

    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    NgbDropdownModule.forRoot(),
    NgbCarouselModule.forRoot(),
    NgbAlertModule.forRoot(),
    CalendarModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCoFeFFLOqbN_oF4Pap1shJx7hID8vMHaw'
    })
  ],
  providers: [BaseService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
