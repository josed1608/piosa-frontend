import {Injectable, Input} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {AppConstants} from './app-constants';
import {Observable} from 'rxjs';

@Injectable()
export class BaseService {

  @Input() selectedObject: any;

  constructor(private http: Http) {
  }

  getArticles(n: number) {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'article/findActive/' + n)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getAnnouncements() {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'announcement/findActive')
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getAdvances() {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'advance/findActive')
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getProyectsByType(typeId: number) {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'project/findActiveByType/' + typeId)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getProfessors() {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'professor/findActive')
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getEvents() {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'event/findActive')
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getSocialActors() {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'socialActor/findActive')
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getWorkTeamData() {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'workTeam/findAllData')
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getCollaboratorsProject(projectId: string) {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'projectCollaborator/findDataByProject/' + projectId)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getCoordinatorsByProject(projectId: string) {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'projectCoordinator/findDataByProject/' + projectId)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getUnitsByProfessor(id: number) {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'professorAcademicUnit/findDataByProfessor/' + id)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getProjectsByProfessor(id: number) {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'project/findByProfessor/' + id)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getProjectsByCollaborator(id: number) {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + 'project/findByCollaborator/' + id)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }

  getEntityById(entity: string, id: number) {
    console.log('Read');
    return this.http.get(AppConstants.API_ENDPOINT + entity + '/findById/' + id)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong' + error);
        }
      );
  }


  sendMailContactForm(socialActor: any) {
    console.log('Create');
    return this.http.post(AppConstants.API_ENDPOINT + 'mailSender/contact',
      socialActor);
  }


}
